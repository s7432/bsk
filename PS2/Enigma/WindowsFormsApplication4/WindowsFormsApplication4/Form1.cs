﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            comboBox1.DataSource = rotor1;
            comboBox2.DataSource = rotor2;
            comboBox3.DataSource = rotor3;
            comboBox4.DataSource = rotor4;
            comboBox5.DataSource = rotor5;
            comboBox6.DataSource = rotor6;
            comboBox7.DataSource = rotor7;
            comboBox8.DataSource = rotor8;
            comboBox9.DataSource = rotorB;
            comboBox10.DataSource = rotorG;


            
        }

     	string[] alfabet =  { "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z" };
        string[] rotor1 =   { "E","K","M","F","L","G","D","Q","V","Z","N","T","O","W","Y","H","X","U","S","P","A","I","B","R","C","J" };
        string[] rotor2 =   { "A","J","D","K","S","I","R","U","X","B","L","H","W","T","M","C","Q","G","Z","N","P","Y","F","V","O","E" };
        string[] rotor3 =   { "B","D","F","H","J","L","C","P","R","T","X","V","Z","N","Y","E","I","W","G","A","K","M","U","S","Q","O" };
        string[] rotor4 =   { "E","S","O","V","P","Z","J","A","Y","Q","U","I","R","H","X","L","N","F","T","G","K","D","C","M","W","B" };
        string[] rotor5 =   { "V","Z","B","R","G","I","T","Y","U","P","S","D","N","H","L","X","A","W","M","J","Q","O","F","E","C","K" };
        string[] rotor6 =   { "J","P","G","V","O","U","M","F","Y","Q","B","E","N","H","Z","R","D","K","A","S","X","L","I","C","T","W" };
        string[] rotor7 =   { "N","Z","J","H","G","R","C","X","M","Y","S","W","B","O","U","F","A","I","V","L","P","E","K","Q","D","T" };
        string[] rotor8 =   { "F","K","Q","H","T","L","X","O","C","B","J","S","P","D","Z","R","A","M","E","W","N","I","U","Y","G","V" };
        string[] rotorB =   { "L","E","Y","J","V","C","N","I","X","W","P","B","Q","M","D","R","T","A","K","Z","G","F","U","H","O","S" };
        string[] rotorG =   { "F","S","O","K","A","N","U","E","R","H","M","B","T","I","Y","C","W","L","Q","P","Z","X","V","G","J","D" };
        string[] rotorsOn = { "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", };

        string[,] reflectorB =      {{"A","Y","B","R","C","U","D","H","E","Q","F","S","G","L","I","P","J","X","K","N","M","O","T","Z","V","W"},
                                     {"Y","A","R","B","U","C","H","D","Q","E","S","F","L","G","P","I","X","J","N","K","O","M","Z","T","W","V"}};

        string[,] reflectorC=       {{"A","F","B","V","C","P","D","J","E","I","G","O","H","Y","K","R","L","Z","M","X","N","W","T","Q","S","U"},
                                     {"F","A","V","B","P","C","J","D","I","E","O","G","Y","H","R","K","Z","L","X","M","W","N","Q","T","U","S"}};

        string[,] reflectorBDunn=   {{"A","E","B","N","C","K","D","Q","F","U","G","Y","H","W","I","J","L","O","M","P","R","X","S","Z","T","V"},
                                     {"E","A","N","B","K","C","Q","D","U","F","Y","G","W","H","J","I","O","L","P","M","X","R","Z","S","V","T"}};

        string[,] reflectorCDunn=    {{"A","R","B","D","C","O","E","J","F","N","G","T","H","K","I","V","L","M","P","W","Q","Z","S","X","U","Y"},
                                     {"R","A","D","B","O","C","J","E","N","F","T","G","K","H","V","I","M","L","W","P","Z","Q","X","S","Y","U"}};

        List<string> wolneLitery = new List<string>();

        public string[] odszyfrujTekst(string[] alfabet_, bool[] rotors, string reflector, string[] tekst)
        {
            string[] tekstOdszyfrowany = tekst;
            string[] log = new string[25];
            log[0] = tekst[0];

            for (int i = 0; i < tekst.Length; i++)
            {
                string tempLine = "";

                foreach (char item in tekst[i])
                {
                    string temp ="";

                    if (System.Text.RegularExpressions.Regex.IsMatch(item.ToString(),"^[A-Za-z]+$")==false)
                    {
                        temp=item.ToString();
                        // dodawanie logu
                        for(int a=0; a<log.Length;a++)
                        {
                            log[a]=log[a]+temp;
                        }
                    }
                    else
                    {
                            temp = Char.ToUpper(item).ToString();
                            
                            // rotory I runda
                            if (rotors[0]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == rotorG[j]) { temp = alfabet[obliczJzPrzesunieciem(j, comboBox10.SelectedIndex, rotorG.Length)]; log[1] = log[1] + temp; break; } } }
                            if (rotors[1]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == rotorB[j]) { temp = alfabet[obliczJzPrzesunieciem(j, comboBox9.SelectedIndex, rotorB.Length)]; log[2] = log[2] + temp; break; } } }
                            if (rotors[2]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == rotor8[j]) { temp = alfabet[obliczJzPrzesunieciem(j, comboBox8.SelectedIndex, rotor8.Length)]; log[3] = log[3] + temp; break; } } }
                            if (rotors[3]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == rotor7[j]) { temp = alfabet[obliczJzPrzesunieciem(j, comboBox7.SelectedIndex, rotor7.Length)]; log[4] = log[4] + temp; break; } } }
                            if (rotors[4]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == rotor6[j]) { temp = alfabet[obliczJzPrzesunieciem(j, comboBox6.SelectedIndex, rotor6.Length)]; log[5] = log[5] + temp; break; } } }
                            if (rotors[5]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == rotor5[j]) { temp = alfabet[obliczJzPrzesunieciem(j, comboBox5.SelectedIndex, rotor5.Length)]; log[6] = log[6] + temp; break; } } }
                            if (rotors[6]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == rotor4[j]) { temp = alfabet[obliczJzPrzesunieciem(j, comboBox4.SelectedIndex, rotor4.Length)]; log[7] = log[7] + temp; break; } } }
                            if (rotors[7]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == rotor3[j]) { temp = alfabet[obliczJzPrzesunieciem(j, comboBox3.SelectedIndex, rotor3.Length)]; log[8] = log[8] + temp; break; } } }
                            if (rotors[8]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == rotor2[j]) { temp = alfabet[obliczJzPrzesunieciem(j, comboBox2.SelectedIndex, rotor2.Length)]; log[9] = log[9] + temp; break; } } }
                            if (rotors[9]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == rotor1[j]) { temp = alfabet[obliczJzPrzesunieciem(j, comboBox1.SelectedIndex, rotor1.Length)]; log[10] = log[10] + temp; break; } } }
                                                                                                 
                            //reflectory
                            if (reflector == "reflectorB") { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == reflectorB[0, j]) {temp = reflectorB[1, j];log[11] = log[11] + temp; break;}}
                            } // reflector B
                            else if (reflector == "reflectorC") { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == reflectorC[0, j]) { temp = reflectorC[1, j]; log[12] = log[12] + temp; break; } } }// reflector C
                            else if (reflector == "reflectorBDunn") { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == reflectorBDunn[0, j]) { temp = reflectorBDunn[1, j]; log[13] = log[13] + temp; break; } } }// reflector B dunn
                            else if (reflector == "reflectorCDunn") { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == reflectorCDunn[0, j]) { temp = reflectorCDunn[1, j]; log[14] = log[14] + temp; break; } } }// reflector C dunn
                            else { MessageBox.Show("Błąd reflektora"); }
                            
                            // rotory II runda
                            if(rotors[9]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor1[j]){temp=alfabet[obliczJzPrzesunieciem(j,comboBox1.SelectedIndex,rotor1.Length)];log[15] = log[15] + temp;break;}} }
                            if(rotors[8]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor2[j]){temp=alfabet[obliczJzPrzesunieciem(j,comboBox2.SelectedIndex,rotor2.Length)];log[16] = log[16] + temp;break;}} }
                            if(rotors[7]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor3[j]){temp=alfabet[obliczJzPrzesunieciem(j,comboBox3.SelectedIndex,rotor3.Length)];log[17] = log[17] + temp;break;}} }
                            if(rotors[6]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor4[j]){temp=alfabet[obliczJzPrzesunieciem(j,comboBox4.SelectedIndex,rotor4.Length)];log[18] = log[18] + temp;break;}} }
                            if(rotors[5]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor5[j]){temp=alfabet[obliczJzPrzesunieciem(j,comboBox5.SelectedIndex,rotor5.Length)];log[19] = log[19] + temp;break;}} }
                            if(rotors[4]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor6[j]){temp=alfabet[obliczJzPrzesunieciem(j,comboBox6.SelectedIndex,rotor6.Length)];log[20] = log[20] + temp;break;}} }
                            if(rotors[3]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor7[j]){temp=alfabet[obliczJzPrzesunieciem(j,comboBox7.SelectedIndex,rotor7.Length)];log[21] = log[21] + temp;break;}} }
                            if(rotors[2]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor8[j]){temp=alfabet[obliczJzPrzesunieciem(j,comboBox8.SelectedIndex,rotor8.Length)];log[22] = log[22] + temp;break;}} }
                            if(rotors[1]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotorB[j]){temp=alfabet[obliczJzPrzesunieciem(j,comboBox9.SelectedIndex,rotorB.Length)];log[23] = log[23] + temp;break;}} }
                            if(rotors[0]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotorG[j]){temp=alfabet[obliczJzPrzesunieciem(j,comboBox10.SelectedIndex,rotorG.Length)];log[24] = log[24] + temp;break;}} }
                    }
                    tempLine = tempLine + temp;
                }

                tekstOdszyfrowany[i]=tempLine;
            }

            // tworzenie pliku z logiem 
            System.IO.File.WriteAllText(@"log.txt", "");
            System.IO.File.WriteAllLines(@"log.txt", log);

            return tekstOdszyfrowany;
        }

        public string[] szyfrujTekst(string[] alfabet_, bool[] rotors, string reflector, string[] tekst)
        {
            string[] tekstZaszyfrowany = tekst;
            string[] log = new string[25];
            log[0] = tekst[0];

            for (int i = 0; i < tekst.Length; i++)
            {
                string tempLine = "";

                foreach (char item in tekst[i])
                {
                    string temp = "";

                    if (System.Text.RegularExpressions.Regex.IsMatch(item.ToString(), "^[A-Za-z]+$") == false)
                    {
                        temp = item.ToString();
                        // dodawanie logu
                        for (int a = 0; a < log.Length; a++)
                        {
                            log[a] = log[a] + temp;
                        }
                    }
                    else
                    {
                        try
                        {
                            temp = Char.ToUpper(item).ToString();

                            // rotory I runda
                            if (rotors[0]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotorG[j],rotorG,comboBox10.SelectedValue.ToString()); log[1] = log[1] + temp; break; } } }
                            if (rotors[1]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotorB[j],rotorB,comboBox9.SelectedValue.ToString()); log[2] = log[2] + temp; break; } } }
                            if (rotors[2]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor8[j],rotor8,comboBox8.SelectedValue.ToString()); log[3] = log[3] + temp; break; } } }
                            if (rotors[3]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor7[j],rotor7,comboBox7.SelectedValue.ToString()); log[4] = log[4] + temp; break; } } }
                            if (rotors[4]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor6[j],rotor6,comboBox6.SelectedValue.ToString()); log[5] = log[5] + temp; break; } } }
                            if (rotors[5]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor5[j],rotor5,comboBox5.SelectedValue.ToString()); log[6] = log[6] + temp; break; } } }
                            if (rotors[6]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor4[j],rotor4,comboBox4.SelectedValue.ToString()); log[7] = log[7] + temp; break; } } }
                            if (rotors[7]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor3[j],rotor3,comboBox3.SelectedValue.ToString()); log[8] = log[8] + temp; break; } } }
                            if (rotors[8]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor2[j],rotor2,comboBox2.SelectedValue.ToString()); log[9] = log[9] + temp; break; } } }
                            if (rotors[9]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor1[j],rotor1,comboBox1.SelectedValue.ToString()); log[10] = log[10] + temp; break; } } }

                            //reflektory
                            if (reflector == "reflectorB") { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == reflectorB[0, j]) { temp = reflectorB[1, j]; log[11] = log[11] + temp; break; } } } // reflector B
                            else if (reflector == "reflectorC") { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == reflectorC[0, j]) { temp = reflectorC[1, j]; log[12] = log[12] + temp; break; } } }// reflector C
                            else if (reflector == "reflectorBDunn") { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == reflectorBDunn[0, j]) { temp = reflectorBDunn[1, j]; log[13] = log[13] + temp; break; } } }// reflector B dunn
                            else if (reflector == "reflectorCDunn") { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == reflectorCDunn[0, j]) { temp = reflectorCDunn[1, j]; log[14] = log[14] + temp; break; } } }// reflector C dunn
                            else { MessageBox.Show("Błąd reflektora"); }

                            // rotory II runda
                            if (rotors[9]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor1[j], rotor1, comboBox1.SelectedValue.ToString()); log[15] = log[15] + temp; break; } } }
                            if (rotors[8]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor2[j], rotor2, comboBox2.SelectedValue.ToString()); log[16] = log[16] + temp; break; } } }
                            if (rotors[7]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor3[j], rotor3, comboBox3.SelectedValue.ToString()); log[17] = log[17] + temp; break; } } }
                            if (rotors[6]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor4[j], rotor4, comboBox4.SelectedValue.ToString()); log[18] = log[18] + temp; break; } } }
                            if (rotors[5]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor5[j], rotor5, comboBox5.SelectedValue.ToString()); log[19] = log[19] + temp; break; } } }
                            if (rotors[4]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor6[j], rotor6, comboBox6.SelectedValue.ToString()); log[20] = log[20] + temp; break; } } }
                            if (rotors[3]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor7[j], rotor7, comboBox7.SelectedValue.ToString()); log[21] = log[21] + temp; break; } } }
                            if (rotors[2]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor8[j], rotor8, comboBox8.SelectedValue.ToString()); log[22] = log[22] + temp; break; } } }
                            if (rotors[1]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotorB[j], rotorB, comboBox9.SelectedValue.ToString()); log[23] = log[23] + temp; break; } } }
                            if (rotors[0]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotorG[j], rotorG, comboBox10.SelectedValue.ToString()); log[24] = log[24] + temp; break; } } }

                        }
                        catch (Exception)
                        { }


                    }
                    tempLine = tempLine + temp;
                }

                tekstZaszyfrowany[i] = tempLine;
            }
            // tworzenie pliku z logiem 
            System.IO.File.WriteAllText(@"log.txt", "");
            System.IO.File.WriteAllLines(@"log.txt", log);

            return tekstZaszyfrowany;
        }

        private string znakPrzesunwPrawo(string znak, string[] rotor, string znakStartowy)
        {
            // określenie przesuniecia
            int przesuniecie = 0;
            for (int i = 0; i < rotor.Length; i++)
            {
                if (rotor[i] == znakStartowy) { przesuniecie = i; break; }
            }

            int indeksZnaku = 0;
            for (int i = 0; i < rotor.Length; i++)
            {
                if (rotor[i] == znak) { indeksZnaku = i; break; }
            }

            int indeksKoncowy = (indeksZnaku + przesuniecie) % rotor.Length;

            return rotor[indeksKoncowy].ToString();
        }

        private string znakPrzesunwLewo(string znak, string[] rotor, string znakStartowy)
        {
            // określenie przesuniecia
            int przesuniecie = 0;
            for (int i = 0; i < rotor.Length; i++)
            {
                if (rotor[i] == znakStartowy) { przesuniecie = i; break; }
            }

            int indeksZnaku = 0;
            for (int i = 0; i < rotor.Length; i++)
            {
                if (rotor[i] == znak) { indeksZnaku = i; break; }
            }

            int indeksKoncowy = (indeksZnaku - przesuniecie + rotor.Length) % rotor.Length;

            return rotor[indeksKoncowy].ToString();
        }

        private int obliczJzPrzesunieciem(int j, int index, int rotor)
        {
            return (j - index + rotor) % rotor;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //1. tworzenie spisu włączonych rotorów
            bool[] rotory = new bool[10];
            if(radioButton1.Checked){rotory[9]=true;} else {rotory[9]=false;} // R1
            if(radioButton4.Checked){rotory[8]=true;} else {rotory[8]=false;} // R2
            if(radioButton6.Checked){rotory[7]=true;} else {rotory[7]=false;} // R3
            if(radioButton8.Checked){rotory[6]=true;} else {rotory[6]=false;} // R4
            if(radioButton10.Checked){rotory[5]=true;} else {rotory[5]=false;} // R5
            if(radioButton12.Checked){rotory[4]=true;} else {rotory[4]=false;} // R6
            if(radioButton14.Checked){rotory[3]=true;} else {rotory[3]=false;} // R7
            if(radioButton16.Checked){rotory[2]=true;} else {rotory[2]=false;} // R8
            if(radioButton18.Checked){rotory[1]=true;} else {rotory[1]=false;} // RB
            if(radioButton20.Checked){rotory[0]=true;} else {rotory[0]=false;} // RG

            //2. określanie deflektora
            string reflektor = "";
            if (radioButton24.Checked){reflektor="reflectorB";}
            else if(radioButton23.Checked){reflektor="reflectorC";}
            else if (radioButton26.Checked){reflektor="reflectorBDunn";}
            else if (radioButton25.Checked){reflektor="reflectorCDunn";}

            string[] tekstArray = new string[1];
            tekstArray[0] = richTextBox1.Text;

            richTextBox2.Text = szyfrujTekst(alfabet, rotory, reflektor, tekstArray)[0];
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //1. tworzenie spisu włączonych rotorów
            bool[] rotory = new bool[10];
            if (radioButton1.Checked) { rotory[9] = true; } else { rotory[9] = false; }
            if (radioButton4.Checked) { rotory[8] = true; } else { rotory[8] = false; }
            if (radioButton6.Checked) { rotory[7] = true; } else { rotory[7] = false; }
            if (radioButton8.Checked) { rotory[6] = true; } else { rotory[6] = false; }
            if (radioButton10.Checked) { rotory[5] = true; } else { rotory[5] = false; }
            if (radioButton12.Checked) { rotory[4] = true; } else { rotory[4] = false; }
            if (radioButton14.Checked) { rotory[3] = true; } else { rotory[3] = false; }
            if (radioButton16.Checked) { rotory[2] = true; } else { rotory[2] = false; }
            if (radioButton18.Checked) { rotory[1] = true; } else { rotory[1] = false; }
            if (radioButton20.Checked) { rotory[0] = true; } else { rotory[0] = false; }

            //2. określanie deflektora
            string reflektor = "";
            if (radioButton24.Checked) { reflektor = "reflectorB"; }
            else if (radioButton23.Checked) { reflektor = "reflectorC"; }
            else if (radioButton26.Checked) { reflektor = "reflectorBDunn"; }
            else if (radioButton25.Checked) { reflektor = "reflectorCDunn"; }

            string[] tekstArray = new string[1];
            tekstArray[0] = richTextBox2.Text;

            richTextBox1.Text = odszyfrujTekst(alfabet, rotory, reflektor, tekstArray)[0];
        }

        private void button5_Click(object sender, EventArgs e)
        {
            logForm logForm = new logForm();
            logForm.Show();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        TextReader tr = new StreamReader(myStream);
                        richTextBox1.Text = tr.ReadToEnd();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // validacja kontrolek
            /*bool valid;
            valid = true;
            if ()*/

            // zapisywanie danych szyfrowania
            string cipherData = "ENIGMA" + System.Environment.NewLine + "used rotors:" + System.Environment.NewLine;
            if (radioButton1.Checked) { cipherData = cipherData + "rotor1; litera starowa: " + comboBox1.SelectedValue.ToString() + System.Environment.NewLine; }
            if (radioButton4.Checked) { cipherData = cipherData + "rotor2; litera starowa: " + comboBox2.SelectedValue.ToString() + System.Environment.NewLine; }
            if (radioButton6.Checked) { cipherData = cipherData + "rotor3; litera starowa: " + comboBox3.SelectedValue.ToString() + System.Environment.NewLine; }
            if (radioButton8.Checked) { cipherData = cipherData + "rotor4; litera starowa: " + comboBox4.SelectedValue.ToString() + System.Environment.NewLine; }
            if (radioButton10.Checked) { cipherData = cipherData + "rotor5; litera starowa: " + comboBox5.SelectedValue.ToString() + System.Environment.NewLine; }
            if (radioButton12.Checked) { cipherData = cipherData + "rotor6; litera starowa: " + comboBox6.SelectedValue.ToString() + System.Environment.NewLine; }
            if (radioButton14.Checked) { cipherData = cipherData + "rotor7; litera starowa: " + comboBox7.SelectedValue.ToString() + System.Environment.NewLine; }
            if (radioButton16.Checked) { cipherData = cipherData + "rotor8; litera starowa: " + comboBox8.SelectedValue.ToString() + System.Environment.NewLine; }
            if (radioButton18.Checked) { cipherData = cipherData + "rotor Beta; litera starowa: " + comboBox9.SelectedValue.ToString() + System.Environment.NewLine; }
            if (radioButton20.Checked) { cipherData = cipherData + "rotor Gamma; litera starowa: " + comboBox10.SelectedValue.ToString() + System.Environment.NewLine; }

            cipherData = cipherData + System.Environment.NewLine + "REFLEKTOR:" + System.Environment.NewLine;

            if (radioButton24.Checked) { cipherData = cipherData + "reflectorB" + System.Environment.NewLine; }
            else if (radioButton23.Checked) { cipherData = cipherData + "reflectorC" + System.Environment.NewLine; }
            else if (radioButton26.Checked) { cipherData = cipherData + "reflectorBDunn" + System.Environment.NewLine; }
            else if (radioButton25.Checked) { cipherData = cipherData + "reflectorCDunn" + System.Environment.NewLine; }

            // zapisywanie plików
            File.WriteAllText("szyfr.txt", richTextBox2.Text);
            File.WriteAllText("szyfr_info.txt", cipherData);

            MessageBox.Show("Zapisuje plik");

            /* nie działa
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "TXT|*.txt";
            saveFileDialog1.Title = "Save encrypted text";
            saveFileDialog1.DefaultExt = ".txt";
            saveFileDialog1.InitialDirectory = Directory.GetCurrentDirectory();
            saveFileDialog1.ShowDialog();
             */
             
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            //string name = saveFileDialog1.FileName;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void comboBoxA_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxA.Items.Clear();
            sprawdzWolneWyrazy();
        }

        public void sprawdzWolneWyrazy()
        {
            // ŁĄCZNICA NIE DZIAŁA

            wolneLitery = new List<string>();

            for(int i=0;i<alfabet.Length;i++)
            {
                bool present = false;
                if (comboBoxA.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxB.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxC.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxD.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxE.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxF.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxG.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxH.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxI.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxJ.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxK.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxL.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxM.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxN.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxO.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxP.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxQ.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxR.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxS.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxT.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxU.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxV.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxW.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxX.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxY.SelectedValue.ToString() == alfabet[i]) { present = true; }
                else if (comboBoxZ.SelectedValue.ToString() == alfabet[i]) { present = true; }

                if (present == true) { wolneLitery.Add(alfabet[i]); }
            }
            
        }
    
    }
}



/* backup


        public string[] odszyfrujTekst(string[] alfabet_, bool[] rotors, string reflector, string[] tekst)
        {
            string[] tekstOdszyfrowany = tekst;
            string[] log = new string[25];
            log[0] = tekst[0];

            for (int i = 0; i < tekst.Length; i++)
            {
                string tempLine = "";

                foreach (char item in tekst[i])
                {
                    string temp ="";

                    if (System.Text.RegularExpressions.Regex.IsMatch(item.ToString(),"^[A-Za-z]+$")==false)
                    {
                        temp=item.ToString();
                        // dodawanie logu
                        for(int a=0; a<log.Length;a++)
                        {
                            log[a]=log[a]+temp;
                        }
                    }
                    else
                    {
                            temp = Char.ToUpper(item).ToString();
                            
                            // rotory I runda
                            if(rotors[0]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotorG[j]){temp=alfabet[j];log[1]=log[1]+temp;break;}} }
                            if(rotors[1]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotorB[j]){temp=alfabet[j];log[2]=log[2]+temp;break;}} }
                            if(rotors[2]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor8[j]){temp=alfabet[j];log[3]=log[3]+temp;break;}} }
                            if(rotors[3]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor7[j]){temp=alfabet[j];log[4]=log[4]+temp;break;}} }
                            if(rotors[4]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor6[j]){temp=alfabet[j];log[5]=log[5]+temp;break;}} }
                            if(rotors[5]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor5[j]){temp=alfabet[j];log[6]=log[6]+temp;break;}} }
                            if(rotors[6]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor4[j]){temp=alfabet[j];log[7]=log[7]+temp;break;}} }
                            if(rotors[7])
                            { 
                                for(int j=0;j<alfabet.Length;j++)
                                {
                                    if(temp.ToString()==rotor3[j])
                                    {
                                        temp=alfabet[j];
                                        log[8]=log[8]+temp;
                                        break;
                                    }
                                } 
                            
                            }
                            if(rotors[8]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor2[j]){temp=alfabet[j];log[9]=log[9]+temp;break;}} }
                            if(rotors[9]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor1[j]){temp=alfabet[j];log[10]=log[10]+temp;break;}} }

                            //reflectory
                            if (reflector == "reflectorB") { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == reflectorB[0, j]) {temp = reflectorB[1, j];log[11] = log[11] + temp; break;}}
                            } // reflector B
                            else if (reflector == "reflectorC") { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == reflectorC[0, j]) { temp = reflectorC[1, j]; log[12] = log[12] + temp; break; } } }// reflector C
                            else if (reflector == "reflectorBDunn") { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == reflectorBDunn[0, j]) { temp = reflectorBDunn[1, j]; log[13] = log[13] + temp; break; } } }// reflector B dunn
                            else if (reflector == "reflectorCDunn") { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == reflectorCDunn[0, j]) { temp = reflectorCDunn[1, j]; log[14] = log[14] + temp; break; } } }// reflector C dunn
                            else { MessageBox.Show("Błąd reflektora"); }
                            
                            // rotory II runda
                            if(rotors[9]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor1[j]){temp=alfabet[j];log[15] = log[15] + temp;break;}} }
                            if(rotors[8]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor2[j]){temp=alfabet[j];log[16] = log[16] + temp;break;}} }
                            if(rotors[7]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor3[j]){temp=alfabet[j];log[17] = log[17] + temp;break;}} }
                            if(rotors[6]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor4[j]){temp=alfabet[j];log[18] = log[18] + temp;break;}} }
                            if(rotors[5]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor5[j]){temp=alfabet[j];log[19] = log[19] + temp;break;}} }
                            if(rotors[4]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor6[j]){temp=alfabet[j];log[20] = log[20] + temp;break;}} }
                            if(rotors[3]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor7[j]){temp=alfabet[j];log[21] = log[21] + temp;break;}} }
                            if(rotors[2]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotor8[j]){temp=alfabet[j];log[22] = log[22] + temp;break;}} }
                            if(rotors[1]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotorB[j]){temp=alfabet[j];log[23] = log[23] + temp;break;}} }
                            if(rotors[0]){ for(int j=0;j<alfabet.Length;j++){if(temp.ToString()==rotorG[j]){temp=alfabet[j];log[24] = log[24] + temp;break;}} }
                    }
                    tempLine = tempLine + temp;
                }

                tekstOdszyfrowany[i]=tempLine;
            }

            // tworzenie pliku z logiem 
            System.IO.File.WriteAllText(@"log.txt", "");
            System.IO.File.WriteAllLines(@"log.txt", log);

            return tekstOdszyfrowany;
        }

        public string[] szyfrujTekst(string[] alfabet_, bool[] rotors, string reflector, string[] tekst)
        {
            string[] tekstZaszyfrowany = tekst;
            string[] log = new string[25];
            log[0] = tekst[0];

            for (int i = 0; i < tekst.Length; i++)
            {
                string tempLine = "";

                foreach (char item in tekst[i])
                {
                    string temp = "";

                    if (System.Text.RegularExpressions.Regex.IsMatch(item.ToString(), "^[A-Za-z]+$") == false)
                    {
                        temp = item.ToString();
                        // dodawanie logu
                        for (int a = 0; a < log.Length; a++)
                        {
                            log[a] = log[a] + temp;
                        }
                    }
                    else
                    {
                        try
                        {
                            temp = Char.ToUpper(item).ToString();

                            // rotory I runda
                            if (rotors[0]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotorG[j],rotorG,comboBox10.SelectedValue.ToString()); log[1] = log[1] + temp; break; } } }
                            if (rotors[1]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotorB[j],rotorB,comboBox9.SelectedValue.ToString()); log[2] = log[2] + temp; break; } } }
                            if (rotors[2]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor8[j],rotor8,comboBox8.SelectedValue.ToString()); log[3] = log[3] + temp; break; } } }
                            if (rotors[3]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor7[j],rotor7,comboBox7.SelectedValue.ToString()); log[4] = log[4] + temp; break; } } }
                            if (rotors[4]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor6[j],rotor6,comboBox6.SelectedValue.ToString()); log[5] = log[5] + temp; break; } } }
                            if (rotors[5]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor5[j],rotor5,comboBox5.SelectedValue.ToString()); log[6] = log[6] + temp; break; } } }
                            if (rotors[6]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor4[j],rotor4,comboBox4.SelectedValue.ToString()); log[7] = log[7] + temp; break; } } }
                            if (rotors[7]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor3[j],rotor3,comboBox3.SelectedValue.ToString()); log[8] = log[8] + temp; break; } } }
                            if (rotors[8]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor2[j],rotor2,comboBox2.SelectedValue.ToString()); log[9] = log[9] + temp; break; } } }
                            if (rotors[9]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor1[j],rotor1,comboBox1.SelectedValue.ToString()); log[10] = log[10] + temp; break; } } }

                            //reflektory
                            if (reflector == "reflectorB") { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == reflectorB[0, j]) { temp = reflectorB[1, j]; log[11] = log[11] + temp; break; } } } // reflector B
                            else if (reflector == "reflectorC") { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == reflectorC[0, j]) { temp = reflectorC[1, j]; log[12] = log[12] + temp; break; } } }// reflector C
                            else if (reflector == "reflectorBDunn") { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == reflectorBDunn[0, j]) { temp = reflectorBDunn[1, j]; log[13] = log[13] + temp; break; } } }// reflector B dunn
                            else if (reflector == "reflectorCDunn") { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == reflectorCDunn[0, j]) { temp = reflectorCDunn[1, j]; log[14] = log[14] + temp; break; } } }// reflector C dunn
                            else { MessageBox.Show("Błąd reflektora"); }

                            // rotory II runda
                            if (rotors[9]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor1[j], rotor1, comboBox1.SelectedValue.ToString()); log[15] = log[15] + temp; break; } } }
                            if (rotors[8]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor2[j], rotor2, comboBox2.SelectedValue.ToString()); log[16] = log[16] + temp; break; } } }
                            if (rotors[7]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor3[j], rotor3, comboBox3.SelectedValue.ToString()); log[17] = log[17] + temp; break; } } }
                            if (rotors[6]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor4[j], rotor4, comboBox4.SelectedValue.ToString()); log[18] = log[18] + temp; break; } } }
                            if (rotors[5]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor5[j], rotor5, comboBox5.SelectedValue.ToString()); log[19] = log[19] + temp; break; } } }
                            if (rotors[4]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor6[j], rotor6, comboBox6.SelectedValue.ToString()); log[20] = log[20] + temp; break; } } }
                            if (rotors[3]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor7[j], rotor7, comboBox7.SelectedValue.ToString()); log[21] = log[21] + temp; break; } } }
                            if (rotors[2]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotor8[j], rotor8, comboBox8.SelectedValue.ToString()); log[22] = log[22] + temp; break; } } }
                            if (rotors[1]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotorB[j], rotorB, comboBox9.SelectedValue.ToString()); log[23] = log[23] + temp; break; } } }
                            if (rotors[0]) { for (int j = 0; j < alfabet.Length; j++) { if (temp.ToString() == alfabet[j]) { temp = znakPrzesunwPrawo(rotorG[j], rotorG, comboBox10.SelectedValue.ToString()); log[24] = log[24] + temp; break; } } }

                        }
                        catch (Exception)
                        { }


                    }
                    tempLine = tempLine + temp;
                }

                tekstZaszyfrowany[i] = tempLine;
            }
            // tworzenie pliku z logiem 
            System.IO.File.WriteAllText(@"log.txt", "");
            System.IO.File.WriteAllLines(@"log.txt", log);

            return tekstZaszyfrowany;
        }


*/